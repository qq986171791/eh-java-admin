package com.viewfin.Utils;


public class Type {
    public Boolean isString(Object o){
        if(o instanceof String){
            return true;
        }
        return false;
    }
    public Boolean isBool(Object o){
        if(o instanceof Boolean){
            return true;
        }
        return false;
    }
    public Boolean isInt(Object o){
        if (o instanceof Integer)
        {
           return true;
        }
        return false;
    }
    public Boolean isFloat(Object o){
        if (o instanceof Float)
        {
            return true;
        }
        return false;
    }
    public Boolean isEnum(Object o){
        if (o instanceof Enum)
        {
            return true;
        }
        return false;
    }
    public Boolean isDouble(Object o){
        if (o instanceof Double)
        {
            return true;
        }
        return false;
    }
}
