package com.viewfin.db;


public class Config {
    private String host = "182.254.243.206";
    private String username = "";
    private String pwd = "";
    private String dbName = "eh";
    private String url = "mongodb://${host}:27017/faya";
    private Integer port = 27017;

    public void onSuccess(){
        System.out.println("db success");
    };

    public void onError(){
        System.out.println("db error");
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
