package com.viewfin.db;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import com.viewfin.TestA;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;


interface IM{
    <T> void add(List<T> lists) throws Exception;
    void del() throws Exception;
    void save() throws Exception;
    <T> T find() throws Exception;
    <T> T select() throws Exception;
}

public class M implements IM {
    Mongo mongo = new Mongo();
    String dbName;
    MongoDatabase db;
    MongoCollection<Document> collection;

    public M(String dbName){
        this.dbName = dbName;
        this.mongo = new Mongo();
        this.mongo.connect();
        this.db = this.mongo.getDb();
        try{
            this.db.createCollection(dbName);
        }catch (Exception e){
            this.collection = this.db.getCollection(dbName);
        }

    }

    public <T> void add(List<T> lists) throws Exception {
        Document document = new Document();

        lists.forEach(d->{
            TestA a = (TestA)d;
            System.out.println(a.getV());
        });

        List<Document> documents = new ArrayList<Document>();
        documents.add(document);
        this.collection.insertMany(documents);
    }

//    public <T> void add(T[] arr) throws Exception {
//        Document document = new Document("title", "MongoDB").
//                append("description", "database").
//                append("likes", 100).
//                append("by", "Fly");
//        List<Document> documents = new ArrayList<Document>();
//        documents.add(document);
//        this.collection.insertMany(documents);
//    }

    public void del() throws Exception {

    }

    public void save() throws Exception {

    }

    public <T> T find() throws Exception {
        return null;
    }

    public <T> T select() throws Exception {
        return null;
    }
}
