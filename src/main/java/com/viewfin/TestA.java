package com.viewfin;

public class TestA{
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getV() {
        return v;
    }

    public void setV(Object v) {
        this.v = v;
    }

    private Object v;

    @Override
    public String toString() {
        return "A{" +
                "index=" + key +
                ", v=" + v +
                '}';
    }

    public TestA(String key, Object v){
        this.key =key;
        this.v = v;
    }
}

